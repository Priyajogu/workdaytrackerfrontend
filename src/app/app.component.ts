import { Component, OnInit } from '@angular/core';
import { MatDialog,MatDialogConfig, MatDialogRef } from '@angular/material/dialog';
import { InputDialogComponent } from 'src/input-dialog/input-dialog.component';
import { EmployeeService } from 'src/services/employee-service';
import { WorkDayService } from 'src/services/workday-service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{

public allEmployees:any;
  
 

  constructor(private employeeService: EmployeeService,private workDayService: WorkDayService,private dialog: MatDialog) {

  }

  ngOnInit(): void {
    this.employeeService.getAllEmployees().subscribe((data)=>{
      this.allEmployees = data;

    });
    throw new Error('Method not implemented.');
  }

  addWorkDays(i : number){ 
    let employee = this.allEmployees[i];
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      message: 'Please Add Working Days Below',
      buttonText: 'Add Work Days',
      employeeId: employee.employeeId,
      allowDecimal: false 
    } 
    const inputDialog = this.dialog.open(InputDialogComponent,dialogConfig);

    inputDialog.afterClosed().subscribe((data)=>{
      return this.workDayService.addWork(employee.employeeID,data).subscribe(data =>{
        this.allEmployees[i]  = data;
      }); 
    });
  }

  takeVacation(i : number){ 
      let employee = this.allEmployees[i];
      const dialogConfig = new MatDialogConfig(); 

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      message: 'Pleae Add Vacation Below',
      buttonText: 'Add Vacation Days',
      employeeId: employee.employeeId,
      allowDecimal: true 
    }

    const inputDialog = this.dialog.open(InputDialogComponent,dialogConfig);

    inputDialog.afterClosed().subscribe((data)=>{
      return this.workDayService.takeVacation(employee.employeeID,data).subscribe(data =>{
        this.allEmployees[i]  = data;
      }); 
    });

  }

  getColor(index: number){

    let employee = this.allEmployees[index];

    return employee.employementType == 0 ?  'coral' :  employee.employeeDesignation == 0 ? 'cornflowerblue' : 'darkseagreen';

  }


  title = 'WorkdayTrackerFrontEnd'; 
}
