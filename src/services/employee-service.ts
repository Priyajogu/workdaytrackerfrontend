import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import {environment} from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class EmployeeService {

    constructor( private http: HttpClient) { }

    getAllEmployees(): Observable<any[]> {
        return this.http.get<any[]>(environment.serviceUrl+'/Employee/GetAllEmployees');
      }

}