import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class WorkDayService {

  constructor( private http: HttpClient) { }

  
  addWork(employeeId:number,WorkDays:number): Observable<any> {
    return this.http.post<any>(environment.serviceUrl+'/WorkDay/AddWorkDays',{WorkDays,employeeId});
  }

  takeVacation(employeeId:number,vacationDays:number): Observable<any> {
    return this.http.post<any>(environment.serviceUrl+'/WorkDay/TakeVacation',{vacationDays,employeeId});
  }



}