import { Component, Inject, OnInit } from "@angular/core"; 
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog"; 
 

@Component({
    selector: 'input-dialog',
    templateUrl: './input-dialog.component.html',
    styleUrls: ['./input-dialog.component.css']
})
export class InputDialogComponent implements OnInit {
    title: any;
    buttonText: any;
    employeeId: any;
    allowDecimal: any; 
    userInput:number = 0;
    
    constructor( 
        private dialogRef: MatDialogRef<InputDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: any) { 
           this.title =  data.message;
           this.buttonText =   data.buttonText;
           this.employeeId =   data.employeeId;
           this.allowDecimal =   data.allowDecimal;  
    }

    ngOnInit() {
 
    }

    onKeyDown(event:any){

        if(event.key==='.' && !this.allowDecimal){event.preventDefault();}
    }
    
    save() {

        this.dialogRef.close(this.userInput);
    }
    close() {
        this.dialogRef.close();
    }
}